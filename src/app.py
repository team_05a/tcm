from flask import Flask, render_template
from blueprints import index_bp,doctor_bp,patient_bp,assistant_bp,feedback_bp
import config

"""
该文件由李树昊实现
"""
app = Flask(__name__,)
app.config.from_object(config)
app.register_blueprint(index_bp)
app.register_blueprint(patient_bp)
app.register_blueprint(doctor_bp)
app.register_blueprint(assistant_bp)
app.register_blueprint(feedback_bp)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

if __name__ == '__main__':
    app.run(debug=True)
