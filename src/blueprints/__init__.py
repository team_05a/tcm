from .patient import bp as patient_bp
from .index import bp as index_bp
from .doctor import bp as doctor_bp
from .assistant import bp as assistant_bp
from .feedback import bp as feedback_bp