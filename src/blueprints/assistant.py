from flask import Blueprint, render_template,request,session,redirect,url_for
import db, datetime

bp = Blueprint('assistant', __name__, url_prefix='/')
time = datetime.date.today()

"""
李树昊写的函数↓ 优化：李佩伦
"""


@bp.route("/assistant",methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        title=''
        category=''
        poeple=''
        date=''
        user_name = session['username']
        context = {'user_name': user_name,'title':title,'category':category,'poeple':poeple,'date':date}
        return render_template("check.html",**context)
    else:
        data = db.Database()
        title = request.form.get('topic')
        category = request.form.get('category')
        content = request.form.get('content')
        people = request.form.get('people')
        date = request.form.get('date')
        doctors = data.get_doctors()
        articles = data.get_articles()
        doctor_id = None
        for doctor in doctors:
            if doctor[1] == people:
                doctor_id = doctor[0]
        if title != None and category != None and content != None and people != None and date != None:
            article_id=None
            for article in articles:
                if article[3] == content:
                    article_id=article[0]
            data.remove_article_by_id(article_id)
            data.insert_article(name=title, author=doctor_id, content=content, tags=category, date=date,status="published")
            print(title)
            print(category)
            print(date)
            print(content)
            print(doctor_id)
            return redirect("/assistant")
        else:
            return redirect("/assistant")


"""
李树昊写的函数↓ 优化：李佩伦
"""


@bp.route("/create_account2", methods=['GET', 'POST'])
def create_account2():
    if request.method == 'GET':
        user_name = session['username']
        context = {'user_name':user_name,'value':'可输入数字\字母\二者混合'}
        return render_template("register.html",**context)
    else:
        user_name = session['username']
        identity = request.form.get('identity')
        username = request.form.get('username')
        password = request.form.get('password')
        confirmPwd = request.form.get("confirmPwd")
        email = request.form.get("email")
        print(identity)
        print(username)
        print(password)
        print(confirmPwd)
        print(email)
        if username == None or password == None or confirmPwd == None or email == None:
            context = {'user_name':user_name,'value': '可输入数字\字母\二者混合'}
            return render_template("register-doctor.html", **context)
        if password != confirmPwd:
            context = {'user_name':user_name,'value': '所输入密码不一致,请重输'}
            return render_template("register.html",**context)
        data = db.Database()
        if identity == "医师":
            data.insert_doctor(username, password)
        elif identity == "医师助理":
            data.insert_assistant(username, password)
        elif identity == "病人":
            data.insert_patient(username,password,email,diagnosis='',tags='')
        return redirect(url_for("index.login"))


"""
李树昊写的函数↓
"""


@bp.route("/check_feedback")
def check_feedback():
    user_name = session['username']
    context = {'user_name': user_name}
    return render_template("feedback.html",**context)


"""
李佩伦写的函数↓
"""


@bp.route("/saved_article")
def saved_article():
    data = db.Database()
    articles = data.get_articles()
    target_article3 = []
    article_time3 = []
    article_sorted_by_time3 = []
    for article in articles:
        if article[6] == "saved":
            target_article3.append(article)
            article_time3.append(article[5])
    article_time_set = set(article_time3)
    article_time_true = list(article_time_set)
    article_time_true.sort(reverse=True)
    for the_time in article_time_true:
        for article in target_article3:
            if article[5] == the_time:
                article_sorted_by_time3.append(article)
    context = {'target_article': target_article3,"article_sorted_by_time3":article_sorted_by_time3}
    return render_template('draft_assistant.html', **context)


"""
李树昊写的函数↓
"""


@bp.route("/assistant/<the_id>", methods=['GET', 'POST'])
def test(the_id):
    if request.method == 'GET':
        data=db.Database()
        a_list = data.get_article_by_id(the_id)
        doctors = data.get_doctors()
        people = None
        for doctor in doctors:
            if doctor[0] == a_list[2]:
                people = doctor[1]
        user_name = session['username']
        context = {'user_name': user_name, 'title': a_list[1], 'category': a_list[4], 'content':a_list[3],'poeple': people, 'date': a_list[5],'the_id':the_id}
        return render_template("check_article_assistant.html", **context)
    else:
        data = db.Database()
        title = request.form.get('topic')
        category = request.form.get('category')
        content = request.form.get('content')
        people = request.form.get('people')
        date = request.form.get('date')
        doctors = data.get_doctors()
        articles = data.get_articles()
        doctor_id = None
        for doctor in doctors:
            if doctor[1] == people:
                doctor_id = doctor[0]
        if title != None and category != None and content != None and people != None and date != None:
            article_id = None
            for article in articles:
                if article[3] == content:
                    article_id = article[0]
            data.remove_article_by_id(article_id)
            data.insert_article(name=title, author=doctor_id, content=content, tags=category, date=date,
                                status='published')
            print(title)
            print(category)
            print(date)
            print(content)
            print(doctor_id)
            return redirect("/assistant")
        else:
            return redirect("/assistant")


"""
李佩伦写的函数↓
"""


@bp.route('/reject_article/<id>',methods=['GET'])
def rejected_article(id):
    data = db.Database()
    article=data.get_article_by_id(id)
    article_name=article[1]
    article_author=article[2]
    article_content=article[3]
    article_tag=article[4]
    article_date=article[5]
    data.remove_article_by_id(id)
    data.insert_article(name=article_name,author=article_author,content=article_content,tags=article_tag,date=article_date,status='rejected')
    return redirect(url_for('assistant.login'))


"""
李树昊写的函数↓
"""


@bp.route('/change_article/<id>',methods=['GET','post'])
def change_article(id):
    data = db.Database()
    title = request.form.get('topic')
    category = request.form.get('category')
    content = request.form.get('content')
    a_list = data.get_article_by_id(id)
    doctors = data.get_doctors()
    doctor_id = None
    for doctor in doctors:
        if doctor[0] == a_list[2]:
            doctor_id = doctor[0]
    print(title)
    print(category)
    print(time)
    print(content)
    print(doctor_id)
    data.insert_article(name=title, author=doctor_id, content=content, tags=category, date=time)
    data.remove_article_by_id(id)
    return redirect(url_for('assistant.login'))