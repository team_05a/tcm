from flask import Blueprint, session,render_template,request,redirect,url_for
import db, datetime

bp = Blueprint('doctor', __name__, url_prefix='/')
time = datetime.date.today()


@bp.route("/doctor",methods=['GET', 'POST'])
def search():
    if request.method == 'GET':
        data = db.Database()
        articles = data.get_articles()
        target_article1 = []
        article_time1 = []
        article_sorted_by_time1 = []
        for article in articles:
            if article[6] == "published":
                target_article1.append(article)
                article_time1.append(article[5])
        article_time_set = set(article_time1)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article1:
                if article[5] == the_time:
                    article_sorted_by_time1.append(article)
        target_article3 = []
        article_time3 = []
        article_sorted_by_time3 = []
        for article in articles:
            if article[6] == "saved":
                target_article3.append(article)
                article_time3.append(article[5])
        article_time_set = set(article_time3)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article3:
                if article[5] == the_time:
                    article_sorted_by_time3.append(article)
        patients=data.get_patients()
        user_name = session['username']
        context = {'patients_len':len(patients), 'articles_len':len(article_sorted_by_time1), 'articles_len_3':len(article_sorted_by_time3), 'user_name':user_name,'contens':'','article_sorted_by_time1':article_sorted_by_time1,'article_sorted_by_time3':article_sorted_by_time3,'patients':patients}
        print(context)
        return render_template("doctormain.html",**context)
    else:
        q = request.form.get("search")
        return redirect(url_for('doctor.search_result', result=q))


"""
李树昊写的函数↓
"""


@bp.route("/create_article",methods=['GET', 'POST'])
def push_article():
    if request.method == 'GET':
        data = db.Database()
        articles = data.get_articles()
        target_article1 = []
        article_time1 = []
        article_sorted_by_time1 = []
        for article in articles:
            if article[6] == "published":
                target_article1.append(article)
                article_time1.append(article[5])
        article_time_set = set(article_time1)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article1:
                if article[5] == the_time:
                    article_sorted_by_time1.append(article)

            print(article_sorted_by_time1)

        target_article2 = []
        article_time2 = []
        article_sorted_by_time2 = []
        for article in articles:
            if article[6] == "rejected":
                target_article2.append(article)
                article_time2.append(article[5])
        article_time_set = set(article_time2)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article2:
                if article[5] == the_time:
                    article_sorted_by_time2.append(article)
        target_article3 = []
        article_time3 = []
        article_sorted_by_time3 = []
        for article in articles:
            if article[6] == "saved":
                target_article3.append(article)
                article_time3.append(article[5])
        article_time_set = set(article_time3)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article3:
                if article[5] == the_time:
                    article_sorted_by_time3.append(article)
        user_name = session['username']
        context = {
            'user_name': user_name,'time':time,
            'article_sorted_by_time1':article_sorted_by_time1, 'articles_len_1':len(article_sorted_by_time1),
            'article_sorted_by_time2':article_sorted_by_time2, 'articles_len_2':len(article_sorted_by_time2),
            'article_sorted_by_time3':article_sorted_by_time3, 'articles_len_3':len(article_sorted_by_time3)
        }
        return render_template("push.html",**context)
    else:
        data = db.Database()
        user_name = session['username']
        title = request.form.get('topic')
        category = request.form.get('category')
        content = request.form.get('content')
        doctors = data.get_doctors()
        doctor_id = None
        for doctor in doctors:
            if doctor[1] == user_name:
                doctor_id = doctor[0]
        print(title)
        print(category)
        print(user_name)
        print(time)
        print(content)
        print(doctor_id)
        data.insert_article(name=title,author=doctor_id,content=content,tags=category,date=time)
        return redirect('/create_article')


"""
李树昊写的函数↓
"""


@bp.route("/create_account1",methods=['GET', 'POST'])
def create_account():
    if request.method == 'GET':
        user_name = session['username']
        context = {'user_name': user_name,'value':'可输入数字\字母\二者混合'}
        return render_template("register-doctor.html",**context)
    else:
        identity = request.form.get('identity')
        username = request.form.get('username')
        password = request.form.get('password')
        confirmPwd = request.form.get("confirmPwd")
        email = request.form.get("email")
        print(identity)
        print(username)
        print(password)
        print(confirmPwd)
        print(email)
        if username == None or password == None or confirmPwd == None or email == None:
            context = {"user_name": session['username'], 'value': '可输入数字\字母\二者混合'}
            return render_template("register-doctor.html", **context)
        if password != confirmPwd:
            context = {"user_name": session['username'], 'value':'所输入密码不一致,请重输'}
            return render_template("register-doctor.html",**context)
        data = db.Database()
        if identity == "医师":
            data.insert_doctor(username, password)
        elif identity == "医师助理":
            data.insert_assistant(username, password)
        elif identity == "病人":
            data.insert_patient(username,password,email,diagnosis='',tags='')
        return redirect(url_for("index.login"))


@bp.route("/pushed_articles")
def pushed_article():
    data = db.Database()
    articles = data.get_articles()
    target_article1 = []
    article_time1 = []
    article_sorted_by_time1 = []
    for article in articles:
        if article[6] == "published":
            target_article1.append(article)
            article_time1.append(article[5])
    article_time_set = set(article_time1)
    article_time_true = list(article_time_set)
    article_time_true.sort(reverse=True)
    for the_time in article_time_true:
        for article in target_article1:
            if article[5] == the_time:
                article_sorted_by_time1.append(article)
    context = {'target_article': target_article1,"article_sorted_by_time1":article_sorted_by_time1}
    return render_template('pushed.html', **context)


@bp.route("/returned_articles")
def returned_article():
    data = db.Database()
    articles = data.get_articles()
    target_article2 = []
    article_time2 = []
    article_sorted_by_time2 = []
    for article in articles:
        if article[6] == "rejected":
            target_article2.append(article)
            article_time2.append(article[5])
    article_time_set = set(article_time2)
    article_time_true = list(article_time_set)
    article_time_true.sort(reverse=True)
    for the_time in article_time_true:
        for article in target_article2:
            if article[5] == the_time:
                article_sorted_by_time2.append(article)
    context = {'target_article': target_article2,"article_sorted_by_time2":article_sorted_by_time2}
    return render_template('return.html',**context)


@bp.route("/draft_articles")
def draft_article():
    data = db.Database()
    articles = data.get_articles()
    target_article3 = []
    article_time3 = []
    article_sorted_by_time3 = []
    for article in articles:
        if article[6] == "saved":
            target_article3.append(article)
            article_time3.append(article[5])
    article_time_set = set(article_time3)
    article_time_true = list(article_time_set)
    article_time_true.sort(reverse=True)
    for the_time in article_time_true:
        for article in target_article3:
            if article[5] == the_time:
                article_sorted_by_time3.append(article)
    context = {'target_article': target_article3,"article_sorted_by_time3":article_sorted_by_time3}
    return render_template('draft.html', **context)


"""
李树昊写的函数↓
"""


@bp.route("/create_article/<the_id>", methods=['GET', 'POST'])
def test(the_id):
    if request.method == 'GET':
        user_name = session['username']
        data = db.Database()
        articles = data.get_articles()
        target_article1 = []
        article_time1 = []
        article_sorted_by_time1 = []
        for article in articles:
            if article[6] == "published":
                target_article1.append(article)
                article_time1.append(article[5])
        article_time_set = set(article_time1)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article1:
                if article[5] == the_time:
                    article_sorted_by_time1.append(article)

        print(article_sorted_by_time1)

        target_article2 = []
        article_time2 = []
        article_sorted_by_time2 = []
        for article in articles:
            if article[6] == "rejected":
                target_article2.append(article)
                article_time2.append(article[5])

        print(article_sorted_by_time2)

        article_time_set = set(article_time2)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article2:
                if article[5] == the_time:
                    article_sorted_by_time2.append(article)
        target_article3 = []
        article_time3 = []
        article_sorted_by_time3 = []
        for article in articles:
            if article[6] == "saved":
                target_article3.append(article)
                article_time3.append(article[5])

        print(article_sorted_by_time3)

        article_time_set = set(article_time3)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article3:
                if article[5] == the_time:
                    article_sorted_by_time3.append(article)
        a_list = data.get_article_by_id(the_id)
        doctors=data.get_doctors()
        people=None
        for doctor in doctors:
            if doctor[0] == a_list[2]:
                people = doctor[1]
        return render_template("check_article.html", **{"title": a_list[1], "tag": a_list[4], "content": a_list[3],"people":people,"date":a_list[5],
                                                        "article_sorted_by_time1": article_sorted_by_time1, "articles_len_1": len(article_sorted_by_time1),
                                                        "article_sorted_by_time2": article_sorted_by_time2, "articles_len_2": len(article_sorted_by_time2),
                                                        "article_sorted_by_time3": article_sorted_by_time3, "articles_len_3": len(article_sorted_by_time3),
                                                        "user_name": user_name,
                                                        "time": time,"the_id":the_id})
    else:
        data = db.Database()
        title = request.form.get('topic')
        category = request.form.get('category')
        content = request.form.get('content')
        a_list = data.get_article_by_id(the_id)
        doctors = data.get_doctors()
        doctor_id = None
        for doctor in doctors:
            if doctor[0] == a_list[2]:
                doctor_id = doctor[0]
        print(title)
        print(category)
        print(time)
        print(content)
        print(doctor_id)
        data.insert_article(name=title,author=doctor_id,content=content,tags=category,date=time)
        return redirect('/create_article')


"""
李树昊写的函数↓
"""


@bp.route("/search_result/<string:result>",methods=['GET', 'POST'])
def search_result(result):
    if request.method == 'GET':
        data = db.Database()
        articles = data.get_articles()
        target_article1 = []
        article_time1 = []
        article_sorted_by_time1 = []
        for article in articles:
            if article[6] == "published":
                target_article1.append(article)
                article_time1.append(article[5])
        article_time_set = set(article_time1)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article1:
                if article[5] == the_time:
                    article_sorted_by_time1.append(article)
        target_article3 = []
        article_time3 = []
        article_sorted_by_time3 = []
        for article in articles:
            if article[6] == "saved":
                target_article3.append(article)
                article_time3.append(article[5])
        article_time_set = set(article_time3)
        article_time_true = list(article_time_set)
        article_time_true.sort(reverse=True)
        for the_time in article_time_true:
            for article in target_article3:
                if article[5] == the_time:
                    article_sorted_by_time3.append(article)
        user_name = session['username']
        context = {'articles_len':len(article_sorted_by_time1), 'articles_len_3':len(article_sorted_by_time3),'user_name': user_name,'contents':result,'article_sorted_by_time1':article_sorted_by_time1,'article_sorted_by_time3':article_sorted_by_time3}
        return render_template("search_result.html",**context)
    else:
        q = request.form.get("search")
        return redirect(url_for('doctor.search_result',result=q))


@bp.route("/result_content/<string:result>")
def search_result_content(result):
    data = db.Database()
    query = []
    patient = data.get_patients()
    article = data.get_articles()
    tag = None
    for patient in patient:
        if patient[1] == result:
            patient_information = data.get_patient_by_name(result)
            query.extend(patient_information)
            tag="patient"
            print(query)
        else:
            continue
    for article in article:
        if result in article[1]:
            article_information = data.get_article_by_name(article[1])
            query.append(article_information)
            tag="article"
            print(query)
        else:
            continue
    context = {'query': query,'tag':tag}
    return render_template('result_content.html',**context)


@bp.route('/article_del/<id>',methods=['GET'])
def article_del(id):
    data = db.Database()
    user_name = session['username']
    target = data.get_article_by_id(id)
    for doctor in data.get_doctors():
        if doctor[0] == target[2]:
            if user_name == doctor[1]:
                data.remove_article_by_id(id)
                return redirect(url_for('doctor.push_article'))
            else:
                return redirect(url_for('doctor.push_article'))


@bp.route('/article_resave/<id>',methods=['GET','post'])
def article_resave(id):
    data = db.Database()
    title = request.form.get('topic')
    category = request.form.get('category')
    content = request.form.get('content')
    a_list = data.get_article_by_id(id)
    doctors = data.get_doctors()
    doctor_id = None
    for doctor in doctors:
        if doctor[0] == a_list[2]:
            doctor_id = doctor[0]
    print(title)
    print(category)
    print(time)
    print(content)
    print(doctor_id)
    data.insert_article(name=title, author=doctor_id, content=content, tags=category, date=time)
    data.remove_article_by_id(id)
    return redirect(url_for('doctor.push_article'))


"""
李树昊写的函数↓
"""


@bp.route('/update_patient_information/<patient_id>',methods=['GET', 'POST'])
def update_patient_information(patient_id):
    if request.method == 'GET':
        user_name = session['username']
        data = db.Database()
        patient = data.get_patient_by_id(patient_id)
        category = patient[5]
        content=patient[4]
        context = {"user_name": user_name, "name": patient[1], "category": category,'time': time,'patient_id':patient_id,'content':content}
        return render_template('edit_patient_information_after_search.html', **context)
    else:
        data=db.Database()
        name=request.form.get('name')
        category=request.form.get('category')
        content=request.form.get('content')
        data.update_diagnosis_by_id(patient_id=patient_id,diagnosis=content)
        patient=data.get_patient_by_id(patient_id)
        data.insert_patient(name=name,password=patient[2],phone=patient[3],diagnosis=content,tags=category)
        data.remove_patient_by_id(patient_id)
        return redirect('/search_for_update')


"""
李树昊写的函数↓
"""


@bp.route('/search_for_update',methods=['GET', 'POST'])
def search_for_update():
    if request.method == 'GET':
        user_name = session['username']
        context = {'user_name': user_name,'content':'','name':'','category':''}
        return render_template("update_patient_information.html", **context)
    else:
        data = db.Database()
        name = request.form.get('name')
        category = request.form.get('category')
        content = request.form.get('content')
        patients = data.get_patients()
        patient_id = None
        for patient in patients:
            if patient[1] == name and patient[5] == category:
                patient_id = patient[0]
        q = request.form.get("search")
        if q != None:
            return redirect(url_for('doctor.search_result_for_update', result=q))
        if name != None and category != None and content != None:
            print(name,category,content)
            patient = data.get_patient_by_id(patient_id)
            data.insert_patient(name=name, password=patient[2], phone=patient[3], diagnosis=content, tags=category)
            data.remove_patient_by_id(patient_id)
            return redirect(url_for('doctor.search_for_update'))
        else:
            return redirect(url_for('doctor.search_for_update'))


"""
李树昊写的函数↓
"""


@bp.route('/search_result_for_update/<string:result>',methods=['GET', 'POST'])
def search_result_for_update(result):
    if request.method == 'GET':
        user_name = session['username']
        context = {'user_name': user_name, 'contents': result}
        return render_template("update_patient_information_after_search.html", **context)
    else:
        data = db.Database()
        name = request.form.get('name')
        category = request.form.get('category')
        content = request.form.get('content')
        patients = data.get_patients()
        patient_id = None
        for patient in patients:
            if patient[1] == name and patient[5] == category:
                patient_id = patient[0]
        if name != None and category != None and content != None:
            print(name,category,content)
            patient = data.get_patient_by_id(patient_id)
            data.insert_patient(name=name, password=patient[2], phone=patient[3], diagnosis=content, tags=category)
            data.remove_patient_by_id(patient_id)
            return redirect(url_for('doctor.search_for_update'))
        else:
            return redirect(url_for('doctor.search_for_update'))


"""
李树昊写的函数↓
"""


@bp.route("/result_content_for_update/<string:result>")
def search_result_content_for_update(result):
    data = db.Database()
    query = []
    patient = data.get_patients()
    for patient in patient:
        if patient[1] == result:
            patient_information = data.get_patient_by_name(result)
            query.extend(patient_information)
            print(query)
        else:
            continue
    context = {'query': query}
    return render_template('result_content_for_update.html',**context)


