from flask import Blueprint, render_template, request, redirect, session, url_for
import db

bp = Blueprint('feedback', __name__)

@bp.route('/feedback/', methods=['GET'], defaults={'id': None})
@bp.route('/feedback/read/<int:id>', methods=['GET'])
def feedback(id):
    if request.method == 'GET':
        if id is None:
            # 查看所有反馈
            username = session.get('username')
            d = db.Database()
            feedbacks = d.get_feedbacks()
            return render_template('feedback.html', feedbacks=feedbacks, id=None, username=username)
        else:
            # 查看指定反馈
            d = db.Database()
            feedback = d.get_feedback_by_id(id)
            return render_template('feedback.html', feedback=feedback, id=id)

@bp.route('/feedback/add/', methods=['GET', 'POST'])
def add_feedback():
    if request.method == 'GET':
        return render_template('feedback.html', add=True)
    elif request.method == 'POST':
        title = request.form['title']
        content = request.form['content']
        print("插入反馈：", type, title, content)
        d = db.Database()
        d.insert_feedback(title, content)
        return redirect("/patient")