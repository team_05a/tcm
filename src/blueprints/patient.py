from flask import Blueprint,render_template,session,request
import db


bp = Blueprint('patient', __name__, url_prefix='/')


@bp.route('/patient',methods=['GET', 'POST'])
def patient():
    user_name = session['username']
    data = db.Database()
    pushed_article = []
    articles = data.get_articles()
    patient = data.get_patient_by_name(name=user_name)
    for article in articles:
        if article[6] == 'published':
            pushed_article.append(article)
    print(articles)
    print(patient)
    ills = patient[0][5].split(";")
    context = {'user_name':user_name,'ill': ills, 'articles': pushed_article}
    return render_template("patientmain.html", **context)


@bp.route('/detailed_information')
def state():
    user_name = session['username']
    context = {'user_name': user_name}
    return render_template("see_article.html", **context)
