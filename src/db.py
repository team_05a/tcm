from datetime import datetime
import sqlite3 as sql


class Database:
    def __init__(self, db="tcm.db"):
        self.conn = sql.connect(db)
        self.cur = self.conn.cursor()
        self.create_table()

    def create_table(self):
        self.cur.execute("""CREATE TABLE IF NOT EXISTS doctors (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                name TEXT NOT NULL,
                password TEXT NOT NULL
            )""")
        self.cur.execute("""CREATE TABLE IF NOT EXISTS patients (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                name TEXT NOT NULL,
                password TEXT NOT NULL,
                phone TEXT,
                diagnosis TEXT,
                tags TEXT
            )""")
        # 文章状态：published 已发布, pending 审核中, rejected 退回, saved 保存
        self.cur.execute("""CREATE TABLE IF NOT EXISTS articles (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                name TEXT NOT NULL,
                author INTEGER NOT NULL,
                content TEXT,
                tags TEXT,
                date TEXT,
                status TEXT DEFAULT 'saved',
                FOREIGN KEY(author) REFERENCES doctors(id)
            )""")
        self.cur.execute("""CREATE TABLE IF NOT EXISTS assistants (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                name TEXT NOT NULL,
                password TEXT NOT NULL
            )""")
        self.cur.execute("""CREATE TABLE IF NOT EXISTS feedbacks (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                title TEXT,
                content TEXT
            )""")
        self.conn.commit()

    def insert_doctor(self, name, password):
        self.cur.execute("INSERT INTO doctors (name,password) VALUES (?,?)", (name, password))
        self.conn.commit()

    def insert_patient(self, name, password, phone, diagnosis, tags):
        self.cur.execute("INSERT INTO patients (name,password,phone,diagnosis,tags) VALUES (?,?,?,?,?)", (name, password, phone, diagnosis, tags))
        self.conn.commit()

    def insert_assistant(self, name, password):
        self.cur.execute("INSERT INTO assistants (name,password) VALUES (?,?)", (name, password))
        self.conn.commit()

    def insert_article(self, name, author, content, tags, date = datetime.now().strftime("%Y-%m-%d %H:%M:%S"), status = 'saved'):
        self.cur.execute("INSERT INTO articles (name,author,content,tags,date,status) VALUES (?,?,?,?,?,?)", (name, author, content, tags, date, status))
        self.conn.commit()

    def insert_feedback(self, title, content):
        self.cur.execute("INSERT INTO feedbacks (title,content) VALUES (?,?)", (title, content))
        self.conn.commit()

    def remove_article_by_id(self, id):
        self.cur.execute("DELETE FROM articles WHERE id = ?", (id,))
        self.conn.commit()

    def remove_patient_by_id(self, id):
        self.cur.execute("DELETE FROM patients WHERE id = ?", (id,))
        self.conn.commit()

# 其中的三个函数(assisitant部分)由李树昊实现↓
    def get_doctors(self):
        self.cur.execute("SELECT * FROM doctors")
        return self.cur.fetchall()

    def get_doctor_by_id(self, id):
        self.cur.execute("SELECT * FROM doctors WHERE id = ?", (id,))
        return self.cur.fetchone()

    def get_doctor_by_name(self, name):
        self.cur.execute("SELECT * FROM doctors WHERE name = ?", (name,))
        return self.cur.fetchone()

    def get_patients(self):
        self.cur.execute("SELECT * FROM patients")
        return self.cur.fetchall()

    def get_patient_by_name(self,name):
        self.cur.execute("SELECT * FROM patients WHERE name = ?", (name,))
        return self.cur.fetchall()
    def get_patient_by_id(self, id):
        self.cur.execute("SELECT * FROM patients WHERE id = ?", (id,))
        return self.cur.fetchone()

    def get_assistants(self):
        self.cur.execute("SELECT * FROM assistants")
        return self.cur.fetchall()

    def get_articles(self):
        self.cur.execute("SELECT * FROM articles")
        return self.cur.fetchall()

    def get_feedbacks(self):
        self.cur.execute("SELECT * FROM feedbacks")
        return self.cur.fetchall()

    def get_article_by_id(self, id):
        self.cur.execute("SELECT * FROM articles WHERE id = ?", (id,))
        return self.cur.fetchone()

    def get_feedback_by_id(self, id):
        self.cur.execute("SELECT * FROM feedbacks WHERE id = ?", (id,))
        return self.cur.fetchone()

    def get_article_by_name(self, name):
        self.cur.execute("SELECT * FROM articles WHERE name = ?", (name,))
        return self.cur.fetchone()

    def get_article_count(self):
        self.cur.execute("SELECT COUNT(*) FROM articles")
        return self.cur.fetchone()[0]

    def update_diagnosis_by_id(self, patient_id, diagnosis):
        self.cur.execute("UPDATE patients SET diagnosis = ? WHERE id = ?", (diagnosis, patient_id))
        self.conn.commit()

    def execute(self, query):
        self.cur.execute(query)
        self.conn.commit()

